# ci-tools-pipeline-project-tools

This module contains complete gitlab ci pipeline for tools project

## pipeline-docker-tools.yml

Since 1.0.0

Complete pipeline for a standard docker tools project

- build and push docker image on gitlab repository (with branche name or tag version as version)
- abstract test job with the pushed docker image
- complete release jobs

### Dependencies

- ci-docker
- release-common
- release-tools
- a project.json file with version

```
{
  "version": "1.0.0"
}
```

### Usage

Exemple for a custom k8s tools image

```
variables:
  DOCKER_BUILD_ARGS: --build-arg CI_K8S_CONFIG="$CI_K8S_CONFIG"

include:
  - project: '$CI_PROJECT_ROOT_NAMESPACE/ci-tools/pipeline/ci-tools-pipeline-docker'
    ref: 1.0.0
    file: '/ci-docker.yml'
  - project: '$CI_PROJECT_ROOT_NAMESPACE/ci-tools/pipeline/ci-tools-pipeline-release'
    ref: 1.0.0
    file: '/release-common.yml'
  - project: '$CI_PROJECT_ROOT_NAMESPACE/ci-tools/pipeline/ci-tools-pipeline-release-tools'
    ref: 1.0.0
    file: '/release-tools.yml'
  - project: '$CI_PROJECT_ROOT_NAMESPACE/ci-tools/pipeline/ci-tools-pipeline-project-tools'
    ref: 1.0.0
    file: '/pipeline-docker-tools.yml'

.test:
  script:
    - kubectl get nodes
```
